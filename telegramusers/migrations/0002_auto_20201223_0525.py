# Generated by Django 3.0 on 2020-12-23 01:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('telegramusers', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='telegramusers',
            name='username',
            field=models.CharField(max_length=100, unique=True),
        ),
    ]
