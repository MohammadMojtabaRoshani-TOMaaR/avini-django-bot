from django.db import models

# Create your models here.
class TelegramUsers(models.Model):
    name = models.CharField('نام و نام خانوادگی',max_length=100)
    username = models.CharField('نام کاربری-تلگرام',max_length=100, unique=True)
    university_id = models.CharField('شماره دانشجویی',max_length=100) 
    #! university_id is a password
    # created_by = models.ForeignKey(User,on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now=True)