from rest_framework import generics, permissions
from rest_framework.response import Response
from .serializer import EmployeeSerializer
from .models import TelegramUsers

class EmployeeCreateApi(generics.CreateAPIView):
    queryset = TelegramUsers.objects.all()
    serializer_class = EmployeeSerializer

class EmployeeApi(generics.ListAPIView):
    permission_classes = [
        permissions.IsAuthenticated,
    ]
    queryset = TelegramUsers.objects.all()
    serializer_class = EmployeeSerializer

class EmployeeUpdateApi(generics.RetrieveUpdateAPIView):
    queryset = TelegramUsers.objects.all()
    serializer_class = EmployeeSerializer

class EmployeeDeleteApi(generics.DestroyAPIView):
    queryset = TelegramUsers.objects.all()
    serializer_class = EmployeeSerializer