from rest_framework import  serializers
from .models import TelegramUsers
from django.contrib.auth.models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('name','username','university_id','created_at')

class EmployeeSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = TelegramUsers
        fields = ('name','username','university_id','created_at')