from django.apps import AppConfig


class TelegramusersConfig(AppConfig):
    name = 'telegramusers'
