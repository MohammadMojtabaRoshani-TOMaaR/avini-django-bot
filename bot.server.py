# @MohammadMojtabaRoshani-TOMaaR

import logging
import requests
import json

from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove, Update, ForceReply
from telegram.ext import (
    Updater,
    CommandHandler,
    MessageHandler,
    Filters,
    ConversationHandler,
    CallbackContext,
)

# Enable logging
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO
)

logger = logging.getLogger(__name__)

GENDER, PHOTO, LOCATION, BIO, SIGNUP, CLASSREGISTER = range(6)
main_class_list = []
univercity_id = ''
wanted_class = ''
classes_links = []


def start(update: Update, context: CallbackContext) -> int:
    # r = requests.get('http://188.34.128.254/api/button')
    # data = json.loads(r.text)
    # # reply_keyboard = data['list']
    # data_length = len(data['list'])
    # key_board_item = []
    # for i in range(data_length):
    #     key_board_item.append(data['list'][i]['name'])

    # reply_keyboard = [key_board_item, ['yeyye']]
    reply_keyboard = [[]]

    update.message.reply_text(
        'سلام دوست من٬ من ربات ثبت نام کارگاه های بسیج دانشجویی دانشگاه شهید باهنر کرمان هستم.'
        '\n\n لطفا با دقت شماره دانشجوییتو به انگلیسی برام بفرست تا چک کنم اگر حساب نداری برات بسازم و '
        'هر وقت خواستی ریست بشم تا دوباره /start کار کنه کنسل /cancel رو بزن',
        reply_markup=ForceReply(True, True)
    )

    return SIGNUP


def signup(update: Update, context: CallbackContext) -> int:
    reply_keyboard = []
    user = update.message.from_user
    user_name = ''
    if type(user.first_name) != type(None) and type(user.last_name) != type(None):
        user_name = 'there is no user name'
    try:
        if type(user.username) != type(None):
            url = "http://188.34.128.254/telegram-users/api/create"
            # payload = "{\n    \"name\": \"test\",\n    \"username\":\"test\",\n    \"university_id\": \"2089040\"\n}"
            a = "{\n    \"name\": \"" + user_name + '\",\n    '
            b = "\"username\":\"" + user.username + "\",\n    "
            c = "\"university_id\": \"" + str(update.message.text) + "\"\n}"
            payload = a + b + c
            headers = {
                'Content-Type': 'application/json'
            }
            response = requests.request(
                "POST", url, headers=headers, data=payload)
            univercity_id = str(update.message.text)
        else:
            update.message.reply_text(
                'برای ساخت حساب کاربیت به یوزر نیمت احتیاج دارم و نمیتونم بهش دسترسی پیدا کنم. لطفا یوزرنیم برای اکانتت یوزر نیم بزار یا دسترسی سایرینو براش ازاد کن',
                reply_markup=ReplyKeyboardRemove(),
            )
            return ConversationHandler.END
    except NameError:
        print(NameError)

    try:
        r = requests.get('http://188.34.128.254/api/button')
        data = json.loads(r.text)
        # reply_keyboard = data['list']
        data_length = len(data['list'])
        key_board_item = []
        global classes_links
        for i in range(data_length):
            key_board_item.append(data['list'][i]['name'])
            classes_links.append(data['list'][i]['link'])
        reply_keyboard = [key_board_item]
        # main_class_list = [key_board_item ]
    except NameError:
        print(NameError)

    if response.status_code == 201:
        print('a')
        update.message.reply_text(
            'اکانتت ساخته شده. حواست باشه نام کاربریت ایدی تلگرامته و رمز عبورت هم شماره دانشجوییته :)',
            reply_markup=ReplyKeyboardMarkup(
                reply_keyboard, one_time_keyboard=True
            ),
        )
        return CLASSREGISTER
    elif(response.status_code == 400):
        update.message.reply_text(
            "تو که اکانت داری پس بزن بریم!",
            reply_markup=ReplyKeyboardMarkup(
                reply_keyboard, one_time_keyboard=True
            ),
        )
        return CLASSREGISTER
    else:
        print('c')
        update.message.reply_text(
            'متاسفانه مشکلی رخ داده. لطفا به @mmroshaniir بگو تا حلش کنه :)',
            reply_markup=ReplyKeyboardRemove(),
        )
    return ConversationHandler.END


def class_handler(update: Update, context: CallbackContext) -> int:
    global wanted_class 
    wanted_class = update.message.text
    print(update.message.text)
    user = update.message.from_user
    update.message.reply_text(
        f'@{user.username} ' +
        '\n\n :لطفا شماره دانشجوییتو با کد ملی بهمراه نام و نام خانوادگیت به این فرمت بهم بده ' +
        '90000000-2000000000-نام ونام خانوادگی',
        reply_markup=ForceReply(True, True)
    )
    return BIO


def skip_photo(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    logger.info("User %s did not send a photo.", user.first_name)
    update.message.reply_text(
        'I bet you look great! Now, send me your location please, ' 'or send /skip.'
    )

    return LOCATION


def location(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    user_location = update.message.location
    # logger.info(
    #     "Location of %s: %f / %f", user.first_name, user_location.latitude, user_location.longitude
    # )
    update.message.reply_text(
        'Maybe I can visit you sometime! ' 'At last, tell me something about yourself.'
    )

    return BIO


def skip_location(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    logger.info("User %s did not send a location.", user.first_name)
    update.message.reply_text(
        'You seem a bit paranoid! ' 'At last, tell me something about yourself.'
    )

    return BIO


def bio(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    print(wanted_class)
    try:
        url = "http://188.34.128.254/class-users/api/create"


        payload = "{\n    \"class_name\": \""+str(wanted_class)+"\",\n    \"user_data\":\""+update.message.text+"\"\n}"
        headers = {
            'Content-Type': 'application/json'
        }

        response = requests.request("POST", url.encode('utf-8'), headers=headers, data=payload)

    except NameError:
        print(NameError)

    if response.status_code == 201:
        update.message.reply_text(
            f'لینک: {classes_links[0]}' + 
            'کلاس با موفقیت برات ثبت شد برو حالشو ببر :)',
            reply_markup=ReplyKeyboardRemove(),
        )
        return CLASSREGISTER
    elif(response.status_code == 400):
        update.message.reply_text(
            "قبلن ثبت نام شدی که !",
            reply_markup=ReplyKeyboardRemove(),
        )
        return CLASSREGISTER
    else:
        update.message.reply_text(
            'متاسفانه مشکلی رخ داده. لطفا به @mmroshaniir بگو تا حلش کنه :)',
            reply_markup=ReplyKeyboardRemove(),
        )
    return ConversationHandler.END


def cancel(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    logger.info("User %s canceled the conversation.", user.first_name)
    update.message.reply_text(
        'مث اینکه کنسل کردی٬ دیگه نه من نه تو!', reply_markup=ReplyKeyboardRemove()
    )

    return ConversationHandler.END


def main() -> None:
    # Create the Updater and pass it your bot's token.
    # Make sure to set use_context=True to use the new context based callbacks
    # Post version 12 this will no longer be necessary
    updater = Updater(
        "977136384:AAGLjc-zr2j0kZXldbN1DVVt6hwUJ1j3ZO0", use_context=True)

    # Get the dispatcher to register handlers
    dispatcher = updater.dispatcher

    # Add conversation handler with the states GENDER, PHOTO, LOCATION and BIO
    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('start', start)],
        states={
            SIGNUP: [MessageHandler(Filters.text & ~Filters.command, signup,)],
            # GENDER: [MessageHandler(Filters.regex('^(Boy|Girl|Other)$'), gender)],
            CLASSREGISTER: [MessageHandler(Filters.text & ~Filters.command, class_handler)],
            LOCATION: [
                MessageHandler(Filters.location, location),
                CommandHandler('skip', skip_location),
            ],
            BIO: [MessageHandler(Filters.text & ~Filters.command, bio)],
        },
        fallbacks=[CommandHandler('cancel', cancel)],
    )

    dispatcher.add_handler(conv_handler)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
