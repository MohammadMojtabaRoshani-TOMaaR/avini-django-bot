from rest_framework import generics, permissions
from rest_framework.response import Response
from .serializer import EmployeeSerializer
from .models import ClassAndUserLink

class EmployeeCreateApi(generics.CreateAPIView):
    queryset = ClassAndUserLink.objects.all()
    serializer_class = EmployeeSerializer

class EmployeeApi(generics.ListAPIView):
    permission_classes = [
        permissions.IsAuthenticated,
    ]
    queryset = ClassAndUserLink.objects.all()
    serializer_class = EmployeeSerializer

class EmployeeUpdateApi(generics.RetrieveUpdateAPIView):
    queryset = ClassAndUserLink.objects.all()
    serializer_class = EmployeeSerializer

class EmployeeDeleteApi(generics.DestroyAPIView):
    queryset = ClassAndUserLink.objects.all()
    serializer_class = EmployeeSerializer