from django.db import models

# Create your models here.
class ClassAndUserLink(models.Model):
    class_name = models.CharField('نام کلاس',max_length=100)
    user_data = models.CharField('مشخصات کاربر',max_length=200) 
    #! university_id is a password
    # created_by = models.ForeignKey(User,on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now=True)