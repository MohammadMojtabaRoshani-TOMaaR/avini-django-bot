from rest_framework import  serializers
from .models import ClassAndUserLink
from django.contrib.auth.models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('class_name','user_data','created_at')

class EmployeeSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = ClassAndUserLink
        fields = ('class_name','user_data','created_at')