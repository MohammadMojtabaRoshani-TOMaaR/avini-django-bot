from .views import GetList
from django.urls import path

urlpatterns = [
    path('button/', GetList.as_view(), name="button-list"),
]